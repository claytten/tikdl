$(document).ready(function () {
    $('#search_btn').on("click", function () {
      $('#result_area').empty();
      let url = $("input").val().trim();

      if (url.toLowerCase().indexOf("tiktok.com/") < 0) {
        alert('Please, just only put tiktok url');
        return;
      }
	  const urlPOST = window.location.origin + '/store'
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: urlPOST,
        method: "POST",
        data: { url : $('input').val() },
        beforeSend: function(request) {
          $('#result_area').append(`
            <div class="loader_bg">
              <div class="loader">
                <h4>Tolong tunggu sebentar...</h4>  
              </div>
            </div>
          `);
          $('#search_btn').prop('disabled', true);
        },
        error: function (xhr, status, error) {
			$('#result_area').empty().append(`
				<div class="loader_bg">
					<div class="loader">
						<h4>Ada kesalahan server. Silahkan download ulang.</h4>  
					</div>
				</div>
			`);
        },
        success: function(res){
          const data = res.data;
		  if(data !== '') {
			$('#result_area').empty().append(`
				<a href="${data}" target="_blank" download class="u-btn u-btn-round u-button-style u-hover-palette-1-light-1 u-palette-1-base u-radius-50 u-btn-2">
					CLICK HERE TO START DOWNLOAD
				</a>
			`)
		  } else {
			$('#result_area').empty().append(`
				<div class="loader_bg">
					<div class="loader">
						<h4>Ada kesalahan server. Silahkan download ulang.</h4>  
					</div>
				</div>
			`);
		  }
        },
        complete: function() {
          $('#search_btn').prop('disabled', false);
        }
      });
    });
  });