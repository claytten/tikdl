<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.landing');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'url' => ['required', 'string'],
        ]);

        // without watermark
        $client = new Client();
        $URI = strval(config('services.tiktok.url'));
        $response = $client->request('GET', $URI, [
            'query' => [
                'url' => $request['url']
            ],
        ]);
        $response = json_decode($response->getBody());

        return response()->json([
            "data" => $response->videoUrl
        ]);
    }

    public function tos() {
        return view('front.tos');
    }

    public function privacy() {
        return view('front.privacy');
    }
}
