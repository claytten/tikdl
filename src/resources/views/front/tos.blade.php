@extends('layouts.front.app')
@section('inline_css')
<style>
  .adsFlex {
    display: flex;
    flex-direction: column;
    height: auto;
    max-width: 970px;
  }
</style>
@endsection
@section('content_body')
<section class="u-align-left u-clearfix u-grey-10 u-section-1" id="carousel_780a" style="padding-bottom:20px">
  <div class="u-clearfix u-sheet u-sheet-1">
    <div class="adsFlex">
      <img src="{{ asset('images/default-image.jpg')}}" alt="">
    </div>
    <h1>Term Of Service</h1>
    <p>
        Set out below are the terms and conditions of your use of, and access to, our website. Please read them carefully, as they impose responsibilities on you in respect of your use of TikTok Video Downloader.
    </p>
    <p>
        By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trademark law.
    </p>
    <h1>Use License</h1>
    <p>
        Permission is granted to temporarily download one copy of the materials (information or software) on {{config('app.name')}}'s website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
    </p>
    <ul>
        <li>Modify or copy the materials;</li>
        <li>Use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>
        <li>Remove any copyright or other proprietary notations from the materials; or</li>
        <li>Transfer the materials to another person or "mirror" the materials on any other server.</li>
    </ul>
    <p>We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.</p>

    <div class="adsFlex">
      <img src="{{ asset('images/default-image.jpg')}}" alt="">
    </div>

    <h1>Disclaimer</h1>
    <p>The materials on {{config('app.name')}}'s website are provided "as is". {{config('app.name')}} makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, {{config('app.name')}} does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.</p>

    <h1>Limitations</h1>
    <p>In no event shall {{config('app.name')}} or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on {{config('app.name')}}'s Internet site, even if {{config('app.name')}} or a {{config('app.name')}} authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.</p>

    <h1>Revisions and Errata</h1>
    <p>The materials appearing on {{config('app.name')}}'s website could include technical, typographical, or photographic errors. {{config('app.name')}} does not warrant that any of the materials on its web site are accurate, complete, or current. {{config('app.name')}} may make changes to the materials contained on its web site at any time without notice. {{config('app.name')}} does not, however, make any commitment to update the materials.</p>

    <h1>Links</h1>
    <p>{{config('app.name')}} has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by {{config('app.name')}} of the site. Use of any such linked web site is at the user's own risk.</p>

    <h1>Site Terms of Use Modifications</h1>
    <p>{{config('app.name')}} may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use. General Terms and Conditions applicable to Use of a Web Site.</p>

    <div class="adsFlex">
      <img src="{{ asset('images/default-image.jpg')}}" alt="">
    </div>
  </div>
</section>

<footer class="u-align-center-sm u-align-center-xs u-clearfix u-footer u-grey-80" id="sec-f281">
  <div class="u-clearfix u-sheet u-sheet-1">
    <div class="u-clearfix u-expanded-width u-gutter-30 u-layout-wrap u-layout-wrap-1">
      <div class="u-gutter-0 u-layout">
        <div class="u-layout-row">
          <div class="u-align-center-sm u-align-center-xs u-align-left-lg u-align-left-md u-align-left-xl u-container-style u-layout-cell u-left-cell u-size-30 u-layout-cell-1">
            <div class="u-container-layout u-container-layout-1">
              <h6 class="u-align-left-sm u-align-left-xs u-text u-text-1">Legal</h6>
              <a href="{{ route('tos')}}" class="u-active-none u-border-2 u-border-palette-1-base u-btn u-btn-rectangle u-button-style u-hover-none u-none u-text-body-alt-color u-btn-1">Term of Service</a>
              <a href="{{ route('privacy')}}" class="u-active-none u-border-2 u-border-palette-1-base u-btn u-btn-rectangle u-button-style u-hover-none u-none u-text-body-alt-color u-btn-2">Privacy Policy</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <p class="u-text u-text-default u-text-4">© 2021 <a href="{{ route('home')}}">TiktokAppDownloader</a>. All Rights Reserved.</p>
  </div>
</footer>
@endsection