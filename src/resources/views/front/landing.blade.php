@extends('layouts.front.app')

@section('inline_css')
<style>
  .adsFlex {
    display: flex;
    flex-direction: column;
    height: auto;
    max-width: 970px;
    margin-top: 12px;
  }
</style>
@endsection
@section('content_body')
<section class="u-align-center u-clearfix u-grey-10 u-section-1" id="carousel_780a" style="padding-bottom:20px">
  <div class="u-clearfix u-sheet u-sheet-1">
    <h1 class="u-custom-font u-font-lato u-text u-text-1">{{config('app.name')}}</h1>
    <div class="adsFlex">
      <img src="{{ asset('images/default-image.jpg')}}" alt="">
    </div>
    <h3 class="u-custom-font u-font-lato u-text u-text-2">Paste your link share here</h3>
    <div class="u-form u-form-1">
      <div class="u-clearfix u-form-horizontal u-form-spacing-10 u-inner-form" style="padding: 10px">
        <div class="u-form-email u-form-group u-form-group-1">
          <input type="text" placeholder="https://www.tiktok.com/@_amirsadboy/video/6975997332022119681?sender_device=pc&sender_web_id=6973315248233842181&is_from_webapp=v1&is_copy_url=0" id="tiktokUrl" class="u-border-4 u-border-white u-input u-input-rectangle u-radius-50 u-white">
        </div>
        <div class="u-align-left u-form-group u-form-submit">
          <button id="search_btn" class="u-active-grey-90 u-border-4 u-border-active-grey-90 u-border-grey-75 u-border-hover-grey-90 u-btn u-btn-round u-btn-submit u-button-style u-grey-75 u-hover-grey-90 u-radius-50 u-btn-1">
            <span class="u-icon u-icon-1">
              <svg class="u-svg-content" viewBox="0 0 512 512" style="width: 1em; height: 1em;">
                <g id="Solid">
                  <path d="m239.029 384.97a24 24 0 0 0 33.942 0l90.509-90.509a24 24 0 0 0 0-33.941 24 24 0 0 0 -33.941 0l-49.539 49.539v-262.059a24 24 0 0 0 -48 0v262.059l-49.539-49.539a24 24 0 0 0 -33.941 0 24 24 0 0 0 0 33.941z"></path><path d="m464 232a24 24 0 0 0 -24 24v184h-368v-184a24 24 0 0 0 -48 0v192a40 40 0 0 0 40 40h384a40 40 0 0 0 40-40v-192a24 24 0 0 0 -24-24z"></path>
                </g>
              </svg>
            </span>
            &nbsp;Download
          </button>
        </div>
      </div>
    </div>
    <div id="result_area">
      
    </div>
  </div>
</section>
<section class="u-align-center u-clearfix u-section-2" id="carousel_2e72" style="padding-bottom:20px">
  <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
    <h2 class="u-text u-text-2">Download TikTok tanpa watermark tanpa aplikasi (gratis)</h2>
    <div class="adsFlex">
      <img src="{{ asset('images/default-image.jpg')}}" alt="">
    </div>
    <h2 class="u-text u-text-3">Cara mengunduh video TikTok tanpa watermark</h2>
    <div class="u-clearfix u-expanded-width u-layout-wrap u-layout-wrap-1">
      <div class="u-layout">
        <div class="u-layout-row">
          <div class="u-container-style u-layout-cell u-size-10-lg u-size-14-sm u-size-14-xs u-size-60-md u-size-9-xl u-layout-cell-1">
            <div class="u-container-layout u-valign-top u-container-layout-1"><span class="u-align-center u-file-icon u-icon u-icon-circle u-palette-1-base u-spacing-10 u-text-white u-icon-1">
              <img src="{{ asset('images/tiktok.png')}}" alt=""></span>
              <h5 class="u-align-center u-text u-text-4">step 1</h5>
              <p class="u-align-center u-text u-text-5">Salin link video TikTok</p>
            </div>
          </div>
          <div class="u-container-style u-layout-cell u-size-6-lg u-size-6-sm u-size-6-xs u-size-60-md u-size-7-xl u-layout-cell-2">
            <div class="u-container-layout u-valign-top u-container-layout-2">
              <span class="u-align-center u-icon u-icon-circle u-text-black u-icon-2">
                <svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 512 512" style="">
                  <use xlink:href="#svg-c606"></use>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" xml:space="preserve" class="u-svg-content" viewBox="0 0 512 512" x="0px" y="0px" id="svg-c606" style="enable-background:new 0 0 512 512;">
                  <g>
                    <g>
                      <path d="M506.134,241.843c-0.006-0.006-0.011-0.013-0.018-0.019l-104.504-104c-7.829-7.791-20.492-7.762-28.285,0.068    c-7.792,7.829-7.762,20.492,0.067,28.284L443.558,236H20c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h423.557    l-70.162,69.824c-7.829,7.792-7.859,20.455-0.067,28.284c7.793,7.831,20.457,7.858,28.285,0.068l104.504-104    c0.006-0.006,0.011-0.013,0.018-0.019C513.968,262.339,513.943,249.635,506.134,241.843z"></path>
                    </g>
                  </g>
                </svg>
              </span>
            </div>
          </div>
          <div class="u-container-style u-layout-cell u-size-10-lg u-size-10-xl u-size-16-sm u-size-16-xs u-size-60-md u-layout-cell-3">
            <div class="u-container-layout u-valign-top u-container-layout-3"><span class="u-align-center u-icon u-icon-circle u-palette-1-base u-spacing-10 u-icon-3"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 512.499 512.499" style=""><use xlink:href="#svg-e5d9"></use></svg><svg class="u-svg-content" viewBox="0 0 512.499 512.499" id="svg-e5d9"><path d="m256.499.251h-.498c-141.386 0-256.001 114.615-256.001 256.001v.498c0 141.108 114.39 255.498 255.498 255.498 10.845 0 20.603-8.433 20.988-19.271.404-11.378-8.699-20.729-19.987-20.729-18.461-1.266-39.37-54.201-46.591-90.979 15.141-3.318 30.743-5.021 46.591-5.021 26.807-1.255 26.431-39.036-.496-40-18.015.034-35.76 1.925-53.01 5.581-2.795-20.742-4.558-42.794-5.21-65.581h294.636c11.089 0 20.078-9 20.075-20.089v-.011c-.057-141.341-114.654-255.895-255.995-255.897zm-164.508 116.163c21.778 18.313 46.165 32.688 72.249 42.754-3.544 24.314-5.739 50.282-6.47 77.081h-116.353c4.192-45.471 22.536-86.9 50.574-119.835zm-50.574 159.834h116.354c.731 26.799 2.926 52.767 6.47 77.081-26.085 10.065-50.472 24.44-72.249 42.754-28.039-32.936-46.383-74.366-50.575-119.835zm79.373 147.932c15.598-12.634 32.71-22.924 50.886-30.684 5.417 23.972 13.766 50.195 24.097 70.063-27.695-8.125-53.098-21.66-74.983-39.379zm60.882-341.158c-3.743 11.182-7.082 23.227-9.996 35.977-18.176-7.761-35.288-18.05-50.886-30.684 21.885-17.718 47.288-31.254 74.983-39.379-4.905 9.511-9.645 20.774-14.101 34.086zm37.931 12.698c15.473-44.751 29.831-55.658 38.088-55.468 18.34 1.612 39.162 54.16 46.354 90.771-30.427 6.881-63.664 6.957-94.137.204 2.793-12.67 6.032-24.565 9.695-35.507zm-21.82 140.528c.652-22.787 2.415-44.84 5.21-65.581 34.998 7.497 73.032 7.422 107.985-.2 2.81 20.799 4.583 42.921 5.236 65.782h-118.431zm134.543-153.226c-4.401-13.149-9.081-24.3-13.922-33.738 27.238 8.161 52.232 21.564 73.807 39.033-15.339 12.429-32.138 22.588-49.976 30.303-2.893-12.609-6.203-24.526-9.909-35.598zm23.901 153.226c-.734-26.93-2.946-53.019-6.521-77.435 25.73-10.046 49.786-24.297 71.303-42.397 28.037 32.936 46.38 74.365 50.572 119.832zm78.527 169.97 35.957-36.313c5.268-5.319 7.123-13.136 4.809-20.255s-8.411-12.351-15.799-13.557l-120-19.585c-6.376-1.036-12.868 1.065-17.42 5.653-4.552 4.589-6.606 11.095-5.514 17.465l20.57 120c1.266 7.386 6.554 13.443 13.7 15.696 7.065 2.242 14.944.347 20.224-5.003l35.328-35.678 71.748 71.749c7.81 7.81 20.474 7.81 28.284 0 7.811-7.811 7.811-20.475 0-28.285zm-64.541 8.331-9.265-54.046 54.046 8.821z"></path></svg></span>
              <h5 class="u-align-center u-text u-text-6">step 2</h5>
              <p class="u-align-center u-text u-text-7">Tempel link video pada form kemudian klik <b>DOWNLOAD</b></p>
            </div>
          </div>
          <div class="u-container-style u-layout-cell u-size-60-md u-size-7 u-layout-cell-4">
            <div class="u-container-layout u-valign-top u-container-layout-4">
              <span class="u-align-center u-icon u-icon-circle u-text-black u-icon-4">
                <svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 512 512" style="">
                  <use xlink:href="#svg-0d64"></use>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" xml:space="preserve" class="u-svg-content" viewBox="0 0 512 512" x="0px" y="0px" id="svg-0d64" style="enable-background:new 0 0 512 512;">
                  <g>
                    <g>
                      <path d="M506.134,241.843c-0.006-0.006-0.011-0.013-0.018-0.019l-104.504-104c-7.829-7.791-20.492-7.762-28.285,0.068    c-7.792,7.829-7.762,20.492,0.067,28.284L443.558,236H20c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h423.557    l-70.162,69.824c-7.829,7.792-7.859,20.455-0.067,28.284c7.793,7.831,20.457,7.858,28.285,0.068l104.504-104    c0.006-0.006,0.011-0.013,0.018-0.019C513.968,262.339,513.943,249.635,506.134,241.843z"></path>
                    </g>
                  </g>
                </svg>
              </span>
            </div>
          </div>
          <div class="u-container-style u-layout-cell u-size-10 u-size-60-md u-layout-cell-5">
            <div class="u-container-layout u-valign-top u-container-layout-5">
              <span class="u-align-center u-icon u-icon-circle u-palette-1-base u-spacing-10 u-icon-5">
                <svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 512 512" style="">
                  <use xlink:href="#svg-d66b"></use>
                </svg>
                <svg class="u-svg-content" viewBox="0 0 512 512" id="svg-d66b">
                  <g>
                    <g>
                      <path d="m223.675 38.386h-96.976c-4.143 0-7.5 3.357-7.5 7.5v139.403c0 4.143 3.357 7.5 7.5 7.5h96.976c4.143 0 7.5-3.357 7.5-7.5v-139.403c0-4.143-3.358-7.5-7.5-7.5zm-7.5 139.403h-81.976v-124.403h81.976z"></path><path d="m260.041 87.731h125.26c4.143 0 7.5-3.357 7.5-7.5v-34.345c0-4.143-3.357-7.5-7.5-7.5h-125.26c-4.143 0-7.5 3.357-7.5 7.5v34.346c0 4.142 3.357 7.499 7.5 7.499zm7.5-34.345h110.26v19.346h-110.26z"></path><path d="m260.041 124.098h125.26c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5h-125.26c-4.143 0-7.5 3.357-7.5 7.5s3.357 7.5 7.5 7.5z"></path><path d="m260.041 156.423h125.26c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5h-125.26c-4.143 0-7.5 3.357-7.5 7.5s3.357 7.5 7.5 7.5z"></path><path d="m260.041 192.789h125.26c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5h-125.26c-4.143 0-7.5 3.357-7.5 7.5s3.357 7.5 7.5 7.5z"></path><path d="m191.931 221.654c0 4.143 3.357 7.5 7.5 7.5h185.87c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5h-185.87c-4.143 0-7.5 3.358-7.5 7.5z"></path><path d="m126.699 229.154h36.366c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5h-36.366c-4.143 0-7.5 3.357-7.5 7.5s3.358 7.5 7.5 7.5z"></path><path d="m228.262 301.981h-20.244c-4.143 0-7.5 3.357-7.5 7.5v69.694c0 1.995.795 3.907 2.209 5.315 1.405 1.399 3.308 2.185 5.291 2.185h.033c.002 0 18.616-.083 23.746-.083 14.479 0 26.26-11.779 26.26-26.259 0-8.648-4.204-16.33-10.673-21.118 2.775-3.862 4.415-8.591 4.415-13.698 0-12.977-10.559-23.536-23.537-23.536zm0 15c4.707 0 8.537 3.83 8.537 8.537s-3.83 8.536-8.537 8.536c-1.517 0-4.401.009-7.526.02h-5.218v-17.093zm3.535 54.612c-2.978 0-10.483.027-16.279.051v-22.549c1.699-.007 3.515-.014 5.257-.02h11.022c6.209 0 11.26 5.051 11.26 11.26 0 6.207-5.051 11.258-11.26 11.258z"></path><path d="m85.387 337.842c-8.755-3.229-16.891-6.751-19.18-7.755-1.763-1.352-2.611-3.469-2.283-5.757.21-1.46 1.252-5.023 6.394-6.572 10.466-3.152 20.042 4.452 20.296 4.656 3.172 2.636 7.878 2.219 10.535-.941 2.665-3.171 2.256-7.902-.915-10.567-.638-.535-15.823-13.058-34.242-7.511-9.165 2.762-15.646 9.967-16.915 18.804-1.187 8.263 2.395 16.21 9.344 20.741.331.216.679.405 1.04.566.401.179 9.964 4.436 20.735 8.408 3.342 1.233 10.997 4.757 9.911 10.839-.783 4.386-5.997 8.923-13.367 8.923-7.264 0-14.266-2.941-18.729-7.867-2.783-3.071-7.525-3.301-10.594-.521-3.069 2.781-3.303 7.524-.521 10.594 7.261 8.012 18.417 12.795 29.845 12.795 14.1 0 25.932-8.952 28.134-21.286 1.647-9.236-2.606-21.322-19.488-27.549z"></path><path d="m127.7 301.981c-4.143 0-7.5 3.357-7.5 7.5v47.957c0 10.218 4.709 18.193 14.396 24.384 5.027 3.213 10.705 4.846 16.875 4.854h.048c5.325 0 10.321-1.156 14.848-3.437 14.267-7.185 17.262-18.023 17.262-25.851v-47.907c0-4.143-3.357-7.5-7.5-7.5s-7.5 3.357-7.5 7.5v47.907c0 3.687-1.023 8.433-9.009 12.454-2.415 1.217-5.141 1.833-8.102 1.833-.009 0-.019 0-.027 0-3.314-.004-6.198-.82-8.818-2.494-5.307-3.391-7.472-6.793-7.472-11.743v-47.957c-.001-4.142-3.358-7.5-7.501-7.5z"></path><path d="m512 276.65c0-15.279-12.426-27.71-27.7-27.71h-53.113v-241.44c0-4.143-3.357-7.5-7.5-7.5h-335.374c-4.143 0-7.5 3.357-7.5 7.5v73.393c0 4.143 3.357 7.5 7.5 7.5s7.5-3.357 7.5-7.5v-65.893h320.373v233.94h-8.832c-4.143 0-7.5 3.357-7.5 7.5s3.357 7.5 7.5 7.5h76.946c7.003 0 12.7 5.702 12.7 12.71v124.692l-50.68-17.806c.881-1.229 1.406-2.732 1.406-4.361v-62.194h11.822c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5h-38.489c-4.143 0-7.5 3.357-7.5 7.5s3.357 7.5 7.5 7.5h11.667v61.778l-36.39-12.786v-56.493c0-4.143-3.357-7.5-7.5-7.5s-7.5 3.357-7.5 7.5v51.223l-17.065-5.996c-1.533-.539-3.159-.538-4.656-.085l-8.673-45.379c-.822-4.036-4.269-7.017-8.382-7.248-4.112-.229-7.87 2.344-9.142 6.261-.027.086-.054.171-.078.258l-13.63 47.812-14.129-47.89c-.026-.089-.054-.176-.083-.264-1.311-3.906-5.094-6.445-9.209-6.167-4.111.275-7.526 3.294-8.32 7.425l-12.25 67.869c-.735 4.076 1.973 7.977 6.049 8.713 4.072.733 7.978-1.972 8.713-6.049l7.806-43.246 12.836 43.507c.026.09.055.179.084.267 1.245 3.704 4.697 6.185 8.602 6.185h.047c3.923-.02 7.373-2.54 8.584-6.271.028-.086.055-.173.08-.261l12.394-43.476 8.289 43.372c.777 4.069 4.714 6.741 8.774 5.958 1.443-.276 2.704-.953 3.703-1.887l14.265 40.599h-265.855c-4.143 0-7.5 3.357-7.5 7.5s3.357 7.5 7.5 7.5h271.124l23.638 67.276c.969 2.758 3.451 4.701 6.361 4.979.239.023.479.034.717.034 2.646 0 5.12-1.4 6.474-3.716l33.15-56.73 20.268-11.844h12.478c15.151 0 27.492-12.227 27.691-27.41 0-.003 0-.006 0-.009.001-.121.009-.24.009-.361zm-69.713 162.946c-1.113.651-2.04 1.578-2.691 2.691l-25.965 44.434-39.591-112.681 112.681 39.591z"></path><path d="m75.081 424.71h-47.381c-7.003 0-12.7-5.697-12.7-12.7v-135.36c0-7.009 5.697-12.71 12.7-12.71h344.585c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5h-276.472v-133.008c0-4.143-3.357-7.5-7.5-7.5s-7.5 3.357-7.5 7.5v133.008h-53.113c-15.274 0-27.7 12.431-27.7 27.71v135.36c0 15.274 12.426 27.7 27.7 27.7h47.381c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5z"></path>
                    </g>
                  </g>
                </svg>
              </span>
              <h5 class="u-align-center u-text u-text-8">step 3</h5>
              <p class="u-align-center u-text u-text-9">Klik tombol <b>Click here to start download</b></p>
            </div>
          </div>
          <div class="u-container-style u-layout-cell u-size-14-sm u-size-14-xs u-size-60-md u-size-7-lg u-size-7-xl u-layout-cell-6">
            <div class="u-container-layout u-valign-top u-container-layout-6">
              <span class="u-align-center u-icon u-icon-circle u-text-black u-icon-4">
                <svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 512 512" style="">
                  <use xlink:href="#svg-0d64"></use>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" xml:space="preserve" class="u-svg-content" viewBox="0 0 512 512" x="0px" y="0px" id="svg-0d64" style="enable-background:new 0 0 512 512;">
                  <g>
                    <g>
                      <path d="M506.134,241.843c-0.006-0.006-0.011-0.013-0.018-0.019l-104.504-104c-7.829-7.791-20.492-7.762-28.285,0.068    c-7.792,7.829-7.762,20.492,0.067,28.284L443.558,236H20c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h423.557    l-70.162,69.824c-7.829,7.792-7.859,20.455-0.067,28.284c7.793,7.831,20.457,7.858,28.285,0.068l104.504-104    c0.006-0.006,0.011-0.013,0.018-0.019C513.968,262.339,513.943,249.635,506.134,241.843z"></path>
                    </g>
                  </g>
                </svg>
              </span>
            </div>
          </div>
          <div class="u-container-style u-layout-cell u-size-10-lg u-size-10-xl u-size-14-sm u-size-14-xs u-size-60-md u-layout-cell-7">
            <div class="u-container-layout u-valign-top u-container-layout-7">
              <span class="u-align-center u-icon u-icon-circle u-palette-1-base u-spacing-10 u-icon-7">
                <svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 512 512" style="">
                  <use xlink:href="#svg-627d"></use>
                </svg>
                <svg class="u-svg-content" viewBox="0 0 512 512" id="svg-627d">
                  <g id="Solid">
                    <path d="m239.029 384.97a24 24 0 0 0 33.942 0l90.509-90.509a24 24 0 0 0 0-33.941 24 24 0 0 0 -33.941 0l-49.539 49.539v-262.059a24 24 0 0 0 -48 0v262.059l-49.539-49.539a24 24 0 0 0 -33.941 0 24 24 0 0 0 0 33.941z"></path><path d="m464 232a24 24 0 0 0 -24 24v184h-368v-184a24 24 0 0 0 -48 0v192a40 40 0 0 0 40 40h384a40 40 0 0 0 40-40v-192a24 24 0 0 0 -24-24z"></path>
                  </g>
                </svg>
              </span>
              <h5 class="u-align-center u-text u-text-10">step 4</h5>
              <p class="u-align-center u-text u-text-11">
                Pengguna dekstop, tekan ctrl+s lalu tentukan folder download <br>
                Pengguna ponsel, tekan icon ":" pada kanan bawah video
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="adsFlex">
      <img src="{{ asset('images/default-image.jpg')}}" alt="">
    </div>
  </div>
</section>

<footer class="u-align-center-sm u-align-center-xs u-clearfix u-footer u-grey-80" id="sec-f281">
  <div class="u-clearfix u-sheet u-sheet-1">
    <div class="u-clearfix u-expanded-width u-gutter-30 u-layout-wrap u-layout-wrap-1">
      <div class="u-gutter-0 u-layout">
        <div class="u-layout-row">
          <div class="u-align-center-sm u-align-center-xs u-align-left-lg u-align-left-md u-align-left-xl u-container-style u-layout-cell u-left-cell u-size-30 u-layout-cell-1">
            <div class="u-container-layout u-container-layout-1">
              <h6 class="u-align-left-sm u-align-left-xs u-text u-text-1">Legal</h6>
              <a href="{{ route('tos')}}" target="_blank" class="u-active-none u-border-2 u-border-palette-1-base u-btn u-btn-rectangle u-button-style u-hover-none u-none u-text-body-alt-color u-btn-1">Term of Service</a>
              <a href="https://napaktilas.net/privacy-policy/" target="_blank" class="u-active-none u-border-2 u-border-palette-1-base u-btn u-btn-rectangle u-button-style u-hover-none u-none u-text-body-alt-color u-btn-2">Privacy Policy</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <p class="u-text u-text-default u-text-4">© 2021 <a href="{{ route('home')}}">TiktokAppDownloader</a>. All Rights Reserved.</p>
  </div>
</footer>
@endsection