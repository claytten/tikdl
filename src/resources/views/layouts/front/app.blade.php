<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="How to download video tiktok without watermark">
    <meta name="description" content="">
    <title>{{ (!empty(config('app.name')) ? config('app.name') : 'Laravel') }}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199984704-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-199984704-1');
    </script>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">

    <!-- Icons -->
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i">

    <!-- Styling -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/front/base.css')}}" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/front/front.css')}}"  media="screen,projection"/>
    @yield('plugins_css')
    @yield('inline_css')
 </head>
 
 <body class="u-body">
    @yield('content_body')
    <!-- Core -->
    <script type="text/javascript" src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/front.js') }}"></script>
    <!-- Optional JS -->
    @yield('plugins_js')
    @yield('inline_js')
  </body>
</html>